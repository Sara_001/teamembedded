#ifndef __BUTTONIMPL__
#define __BUTTONIMPL__

#include "Button.hpp"

class ButtonImpl: public Button {

public:
  ButtonImpl(int pin);
  bool isPressed();

private:
	unsigned short int pin;

};

#endif
