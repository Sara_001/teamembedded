/*
 * PirSensor.hpp
 *
 *  Created on: 10 nov 2017
 *      Author: Rad
 */

#ifndef __PIRSENSOR_HPP__
#define __PIRSENSOR_HPP__

#include "Arduino.h"
class PirSensor {
	unsigned short int pinPir;
public:
	PirSensor(int pinSensor);
	virtual bool checkArea();
};
#endif /* PIRSENSOR_HPP_ */
