/*
 * PirSensor.cpp
 *
 *  Created on: 10 nov 2017
 *      Author: Rad
 */
#include "PirSensor.hpp"
PirSensor::PirSensor(int pinSensor) {
	this->pinPir = pinSensor;
	pinMode(pinSensor, INPUT);
}

bool PirSensor::checkArea() {
	return digitalRead(this->pinPir);
}


