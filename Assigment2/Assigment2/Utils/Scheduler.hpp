/*
 * Scheduler.hpp
 *
 *  Created on: 07 nov 2017
 *      Author: Rad
 */

#ifndef __SCHEDULER_HPP__
#define __SCHEDULER_HPP__


#include <list>

#include "../Task/Task.hpp"
#include "Timer.hpp"

#define MAX_TASK 20

class Scheduler {
	bool ready;
	unsigned short int Period;
	std::list<Task*> listTask;
	Timer timer;


public:

	Scheduler(int period);

	virtual bool addTask(Task* task);


	virtual void clock();

//	virtual ~Scheduler();

};

#endif /* SCHEDULER_HPP_ */
