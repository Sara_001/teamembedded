/*
 * Counterinput.cpp
 *
 *  Created on: 12 nov 2017
 *      Author: Rad
 */
#include "CounterInput.hpp"



CounterInput::CounterInput(int limit) {
	this->times = 0;
	this->thresholdReveal = limit;
}

bool CounterInput::addResult(bool input) {
	if (input) {
		if (this->times++ >= this->thresholdReveal) {
			this->times = 0;
			return true;
		}

	}
	return false;
}

void CounterInput::reset() {
	this->times = 0;
}
