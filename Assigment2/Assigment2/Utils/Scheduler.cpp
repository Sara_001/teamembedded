#include "Scheduler.hpp"


bool Scheduler::addTask(Task* task) {
	if (this->listTask.size() <= MAX_TASK) {
		this->listTask.push_front(task);
		return true;
	} else {
		return false;
	}
}

Scheduler::Scheduler(int period) {
	this->ready = false;
	this->timer.setupPeriod(period);
	this->Period = period;
}

void Scheduler::clock() {
	this->timer.waitForNextTick();
	for (Task* i : listTask) {
		if (i->updateAndCheckTime(this->Period)) {
			i->tick();
		}
	}
}
