/*
 * Counterinput.hpp
 *
 *  Created on: 12 nov 2017
 *      Author: Rad
 */

#ifndef __COUNTERINPUT_HPP__
#define __COUNTERINPUT_HPP__
#include "Arduino.h"

class CounterInput {
public:

	CounterInput(int limit);

	virtual bool addResult(bool input);

	virtual void reset();

private:
	unsigned short int thresholdReveal;
	unsigned short int times;

};






#endif /* COUNTERINPUT_HPP_ */
