/*
 * DataCenter.hpp
 *
 *  Created on: 11 nov 2017
 *      Author: Rad
 */

#ifndef __DATACENTER_HPP__
#define __DATACENTER_HPP__
class DataCenter {

public:
	static DataCenter* getInstance();
	void setparkingCorrect(bool value);
	void setNoticeCar(bool value);
	void setClosing(bool value);
	void reset();

	bool getParkingCorrect();
	bool getNoticeCar();
	bool getClosing();
protected:
	DataCenter() {
		noticeCar = false;
		parkingCorrect = false;
		closing = false;
	}
	;
private:
	static DataCenter* instance;
	bool parkingCorrect;
	bool noticeCar;
	bool closing;
	void operator=(const DataCenter&) = delete;
};

#endif /* DATACENTER_HPP_ */
