
#include "DataCenter.hpp"
DataCenter* DataCenter::instance = 0;

DataCenter* DataCenter::getInstance() {
	if (!instance) {
		instance = new DataCenter;
	}
	return instance;
}


void DataCenter::setNoticeCar(bool value) {
	this->noticeCar = value;
}
void DataCenter::setparkingCorrect(bool value) {
	this->parkingCorrect = value;
}

void DataCenter::setClosing(bool value) {
	this->closing = value;
}

void DataCenter::reset() {
	this->closing = false;
	this->noticeCar = false;
	this->parkingCorrect = false;
}

bool DataCenter::getNoticeCar() {
	return this->noticeCar;
}

bool DataCenter::getParkingCorrect() {
	return this->parkingCorrect;
}

bool DataCenter::getClosing() {
	return this->closing;
}

