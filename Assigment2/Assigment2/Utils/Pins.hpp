/*
 * Pins.hpp
 *
 *  Created on: 13 nov 2017
 *      Author: Rad
 */

#ifndef __PINS_HPP__
#define __PINS_HPP__


#define BUTT_1 13
#define BUTT_2 4
#define LED_R 6
#define LED_G1 5
#define LED_G2 3
#define PROX_TRIGG 10
#define PROX_ECHO 9
#define PIR_SIG 7



#endif /* PINS_HPP_ */
