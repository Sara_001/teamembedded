/*
 * TaskApproach.cpp
 *
 *  Created on: 09 nov 2017
 *      Author: Rad
 */
#include "TaskApproach.hpp"


/*TaskApproach::TaskApproach(int distPin, int ledMin, int ledMax)
Led ledMin(=)
 }*/


void TaskApproach::init(int time) {
	this->triggTime = time;
}



void TaskApproach::tick() {

	if (!DataCenter::getInstance()->getClosing()) {
		if (DataCenter::getInstance()->getNoticeCar()) {
			float dist = this->sonar->getDistance();
			MsgService.sendMsg(((String) dist));

			if (dist <= DISTMIN) {
				this->counter2->reset();
				this->counter3->reset();
				this->counter4->reset();
				if (this->counter1->addResult(true) && !this->seen) {
					MsgService.sendMsg("OK CAN STOP");
					this->seen = true;
					this->lg1->switchOn();
					this->lg2->switchOn();
					DataCenter::getInstance()->setparkingCorrect(true);
				}
			} else if (dist > DISTMIN && dist < DISTCLOSE) {
				this->counter1->reset();
				this->counter3->reset();
				this->counter4->reset();
				if (this->counter2->addResult(true)) {
					this->seen = false;
					this->lg1->switchOn();
					this->lg2->switchOff();
					DataCenter::getInstance()->setparkingCorrect(true);
				}
			} else if (dist < DISTMAX && dist >= DISTCLOSE) {
				this->counter2->reset();
				this->counter3->reset();
				this->counter1->reset();
				if (this->counter4->addResult(true)) {
					this->seen = false;
					this->lg1->switchOn();
					this->lg2->switchOff();
					DataCenter::getInstance()->setparkingCorrect(false);
				}
			} else if (dist >= DISTMAX) {
				this->counter1->reset();
				this->counter2->reset();
				this->counter4->reset();
				if (this->counter3->addResult(true)) {
					this->seen = false;
					this->lg1->switchOff();
					this->lg2->switchOff();
					DataCenter::getInstance()->setparkingCorrect(false);
				}
			}
		}
	} else {
		this->lg1->switchOff();
		this->lg2->switchOff();
	}
}

TaskApproach::TaskApproach(int echoPin, int trigPin, int pinlg1, int pinlg2) :
		Task() {
	this->seen = false;
	this->sonar = new Sonar(echoPin, trigPin);
	this->lg1 = new Led(pinlg1);
	this->lg2 = new Led(pinlg2);
	this->counter1 = new CounterInput(TRESHOLD_REVEALS);
	this->counter2 = new CounterInput(TRESHOLD_REVEALS);
	this->counter3 = new CounterInput(TRESHOLD_REVEALS);
	this->counter4 = new CounterInput(TRESHOLD_REVEALS);
}

