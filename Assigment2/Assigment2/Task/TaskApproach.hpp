
#ifndef __TASKAPPROACH_HPP__
#define __TASKAPPROACH_HPP__
#include "Task.hpp"
#include "../Sensor/Sonar.hpp"
#include "../Sensor/LedExt.hpp"
#include "../Sensor/Led.hpp"
#include "Arduino.h"
#include "../Utils/DataCenter.hpp"
#include "../Utils/CounterInput.hpp"
#include "../Utils/MsgService.hpp"
#define DISTMIN 0.1
#define DISTMAX 1
#define DISTCLOSE  0.5
#define TRESHOLD_REVEALS 4


class TaskApproach: public Task {
public:
	void tick();
	void init(int time);
	TaskApproach(int echoPin, int trigPin, int pinlg1, int pinlg2);
private:
	bool seen;
	Sonar* sonar;
	Led* lg1;
	CounterInput* counter1;
	CounterInput* counter2;
	CounterInput* counter3;
	CounterInput* counter4;
	Led* lg2;
};
#endif /* TASKAPPROACH_HPP_ */
