/*
 * TouchTask.hpp
 *
 *  Created on: 11 nov 2017
 *      Author: Rad
 */

#ifndef __CLOSEBUTTONTASK_HPP__
#define __CLOSEBUTTONTASK_HPP__
#include "Task.hpp"
#include  "Arduino.h"
#include "../Utils/DataCenter.hpp"
#include "../Sensor/ButtonImpl.hpp"
#include "../Utils/MsgService.hpp"

class CloseButtonTask: public Task {
	ButtonImpl* butt;

public:
	virtual void init(int time);
	virtual void tick();
	CloseButtonTask(int buttPin);

};



#endif /* TOUCHTASK_HPP_ */
