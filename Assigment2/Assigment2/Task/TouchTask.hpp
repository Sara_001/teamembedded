/*
 * TouchTask.hpp
 *
 *  Created on: 11 nov 2017
 *      Author: Rad
 */

#ifndef __TOUCHTASK_HPP__
#define __TOUCHTASK_HPP__
#include "Task.hpp"
#include  "Arduino.h"
#include "../Sensor/ButtonImpl.hpp"
#include "../Utils/MsgService.hpp"

class TouchTask: public Task {
	ButtonImpl* butt;

public:
	virtual void init(int time);
	virtual void tick();
	TouchTask(int buttPin);

};



#endif /* TOUCHTASK_HPP_ */
