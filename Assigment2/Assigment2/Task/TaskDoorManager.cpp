/*

 * GarageTask.cpp
 *
 *  Created on: 08 nov 2017
 *      Author: Rad
 */

#include "TaskDoorManager.hpp"

#include <HardwareSerial.h>


DoorManager::DoorManager(int pinLed, int pinPir) :
		Task() {
	this->fadeValue = 0;
	this->state = IDLE;
	this->pirs = new PirSensor(pinPir);
	this->ledR = new LedExt(pinLed, 0);
	this->ledR->switchOn();
	this->counter = new CounterInput(TRESHOLD_REVEAL);
	this->time = 0;
}

void DoorManager::init(int time) {
	this->triggTime = time;
}

void DoorManager::tick() {

	switch (state) {
	case APERTURA:
		//Serial.println("APERTURA");
		if (this->fadeValue < MAX_LUM) {
			this->fadeValue += FADE_AMOUNT;
			this->ledR->setIntensity(this->fadeValue);
		} else {
			this->state = AVVICINAMENTO;
		}
		break;
	case CHIUSURA:
		///Serial.println("CHIUSURA");
		if (this->fadeValue >= FADE_AMOUNT) {
			this->fadeValue -= FADE_AMOUNT;
			this->ledR->setIntensity(this->fadeValue);
		} else {
			this->state = IDLE;
			DataCenter::getInstance()->reset();
		}
		break;
	case AVVICINAMENTO:

		this->time += this->triggTime;
		if (this->time >= SEC_WAIT * MULTIPLAYER) {
			MsgService.sendMsg("CLOSING");
			this->state = CHIUSURA;
			this->time = 0;
		}


		if (this->counter->addResult(this->pirs->checkArea())) {
				DataCenter::getInstance()->setNoticeCar(true);
			Serial.println("Welcome Home");
			this->time = 0;
				this->state = IDLE;
		} else if (DataCenter::getInstance()->getClosing()) {
			this->state = CHIUSURA;
		}
		break;
	case IDLE:

		if (DataCenter::getInstance()->getClosing()) {
			this->state = CHIUSURA;

		} else if (MsgService.isMsgAvailable()
				&& (MsgService.receiveMsgNocanc()->getContent() == "OPEN")) {

				MsgService.CancelMessage();
			this->state = APERTURA;
		}

		break;
	}

}








