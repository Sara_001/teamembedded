/*
 * TouchTask.cpp
 *
 *  Created on: 11 nov 2017
 *      Author: Rad
 */

#include "CloseButtonTask.hpp"
CloseButtonTask::CloseButtonTask(int buttPin) {
	this->butt = new ButtonImpl(buttPin);
}

void CloseButtonTask::tick() {

	if ((this->butt->isPressed()
			&& ((DataCenter::getInstance()->getParkingCorrect())
					|| !DataCenter::getInstance()->getNoticeCar()))) {
		DataCenter::getInstance()->setClosing(true);
		MsgService.sendMsg("CLOSING");
	}


}

void CloseButtonTask::init(int time) {
	this->triggTime = time;
}


