
#ifndef __TASKCARSTOP_HPP__
#define __TASKCARSTOP_HPP__
#include "Task.hpp"
#include  "Arduino.h"
#include  "../Utils/DataCenter.hpp"
#include "../Utils/MsgService.hpp"


class TaskCarStop: public Task {
public:
	virtual void init(int time);
	virtual void tick();
	TaskCarStop();
};



#endif /* TOUCHTASK_HPP_ */
