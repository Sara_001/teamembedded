/*
 * TouchTask.cpp
 *
 *  Created on: 11 nov 2017
 *      Author: Rad
 */

#include "TouchTask.hpp"

TouchTask::TouchTask(int buttPin) {
	this->butt = new ButtonImpl(buttPin);
}

void TouchTask::tick() {
	if (this->butt->isPressed()) {
		MsgService.sendMsg("TOUCHING");

	}
}

void TouchTask::init(int time) {
	this->triggTime = time;
}


