/*
 * GarageTask.hpp
 *
 *  Created on: 08 nov 2017
 *      Author: Rad
 */

#ifndef __GARAGETASK_HPP__
#define __GARAGETASK_HPP__
#include "Task.hpp"
#include  "Arduino.h"

#include "../Utils/CounterInput.hpp"
#include "../Sensor/PirSensor.hpp"
#include "../Sensor/LedExt.hpp"
#include "../Utils/DataCenter.hpp"
#include "../Utils/MsgService.hpp"
#define FADE_AMOUNT 1
#define MAX_LUM 255
#define PIR_SIG 7
#define TRESHOLD_REVEAL 5
#define	SEC_WAIT 5
#define MULTIPLAYER 1000

enum State {
	APERTURA, CHIUSURA, AVVICINAMENTO, IDLE
};

class DoorManager: public Task {
public:
	virtual void init(int time);
	virtual void tick();
	DoorManager(int pinLed, int pinPir);
private:
	State state;
	unsigned int time;
	LedExt* ledR;
	PirSensor * pirs;
	unsigned short int fadeValue;
	CounterInput* counter;
};
#endif /* GARAGETASK_HPP_ */
