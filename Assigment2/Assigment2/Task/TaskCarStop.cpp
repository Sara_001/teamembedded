
#include "TaskCarStop.hpp"


void TaskCarStop::tick() {
	if (MsgService.isMsgAvailable()
			&& (MsgService.receiveMsgNocanc()->getContent() == "CLOSE")) {
		MsgService.CancelMessage();
		if (DataCenter::getInstance()->getParkingCorrect()) {
			MsgService.sendMsg("OK");
		} else {
			MsgService.sendMsg("TOO FAR");
		}

	}
}

void TaskCarStop::init(int time) {
	this->triggTime = time;
}


TaskCarStop::TaskCarStop() {
}
;
