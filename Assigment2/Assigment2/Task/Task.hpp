/*
 * Task.hpp
 *
 *  Created on: 08 nov 2017
 *      Author: Rad
 */

#ifndef __TASK_HPP__
#define __TASK_HPP__


class Task {
protected:
	unsigned short int triggTime;
	unsigned short int timeElapsed;
public:
	virtual void init(int time) = 0;
	virtual void tick() = 0;
	Task() {
		this->timeElapsed = 0;
		this->triggTime = 0;
	}
	bool updateAndCheckTime(int basePeriod) {
		timeElapsed += basePeriod;
		if (timeElapsed >= triggTime) {
			timeElapsed = 0;
			return true;
		} else {
			return false;
		}
	}


};


#endif /* TASK_HPP_ */
