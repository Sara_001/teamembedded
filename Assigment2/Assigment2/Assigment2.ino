#include "Arduino.h"
#include "Utils/Scheduler.hpp"
#include "Task/TaskApproach.hpp"
#include "Utils/Timer.hpp"
#include  "Task/TouchTask.hpp"
#include "Task/CloseButtonTask.hpp"
#include "Utils/MsgService.hpp"
#include "Task/TaskDoorManager.hpp"
#include "Task/TaskCarStop.hpp"
#include "Utils/Pins.hpp"

DoorManager* doorMnager;
Scheduler * sched;
TaskApproach* approach;
TouchTask* touch;
CloseButtonTask* close;
TaskCarStop* carStop;

void setup()
{
	MsgService.init();
	pinMode(BUTT_1, INPUT);
	pinMode(BUTT_2, INPUT);
	pinMode(LED_G1, OUTPUT);
	pinMode(LED_G2, OUTPUT);
	doorMnager = new DoorManager(LED_R, PIR_SIG);
	approach = new TaskApproach(PROX_ECHO, PROX_TRIGG, LED_G1, LED_G2);
	sched = new Scheduler(10);
	close = new CloseButtonTask(BUTT_2);
	touch = new TouchTask(BUTT_1);
	carStop = new TaskCarStop();

	doorMnager->init(10);
	carStop->init(30);
	approach->init(180);
	close->init(240);
	touch->init(280);
	sched->addTask(carStop);
	sched->addTask(doorMnager);
	sched->addTask(approach);
	sched->addTask(touch);
	sched->addTask(close);


}

void loop()
{

	sched->clock();

//Add your repeated code here
}
