package jparking.Model;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import gnu.io.CommPortIdentifier;

public class SerialUtil {

    public static List<String> serialsName() {
        final List<String> lis = new ArrayList<>();
        final Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        while (portEnum.hasMoreElements()) {
            final CommPortIdentifier currPortId = (CommPortIdentifier) portEnum
                    .nextElement();
            lis.add(currPortId.getName());
        }
        return lis;
    }

}
