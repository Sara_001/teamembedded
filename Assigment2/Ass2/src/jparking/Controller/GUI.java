package jparking.Controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import jparking.Model.Observ;
import jparking.Model.SerialMonitor;
import jparking.Model.SerialUtil;

public class GUI implements Observ {

    private final JFrame            gui;
    private final JButton           open;
    private final JButton           stop;
    private final JButton           connect;
    private final JLabel            messages;
    private String                  currentDist;
    private final JLabel            distance;
    private final JComboBox<String> listBound;
    private final JComboBox<String> listPort;
    private final SerialMonitor     ser;
    // private final Model model;

    /**
     * Builds a new {@link GUI}.
     *
     * @throws Exception
     */

    public static void main(final String[] a) throws IOException {
        new GUI();
    }

    public GUI() {
        this.ser = new SerialMonitor(null);
        this.ser.addObserver(this);

        this.gui = new JFrame();
        this.gui.setTitle("JParking");
        this.open = new JButton("Open");
        this.stop = new JButton("Stop");
        this.connect = new JButton("Connect");
        this.messages = new JLabel(
                "This is where you'll see the messages");
        this.currentDist = "XXX";
        this.distance = new JLabel("Distance: " + this.currentDist);
        this.listBound = new JComboBox<>(new String[] { "9600", "300", "1200", "2400", "4800", "14400", "19200",
                "28800", "31250", "38400", "57600", "76800", "115200", "230400", "460800", "921600" });
        this.listPort = new JComboBox<>();

        for (final String elem : SerialUtil.serialsName()) {
            this.listPort.addItem(elem);
        }

        final JPanel mainPanel = new JPanel();
        final JPanel inputPanel = new JPanel();
        final JPanel textPanel = new JPanel();
        final JPanel distPanel = new JPanel();

        this.gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.gui.setSize((int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 3),
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 5);

        mainPanel.setLayout(new BorderLayout());
        inputPanel.setLayout(new FlowLayout());
        inputPanel.add(this.open);
        inputPanel.add(this.stop);
        distPanel.add(this.distance);
        textPanel.setLayout(new BorderLayout());
        textPanel.add(this.messages, BorderLayout.CENTER);
        inputPanel.add(this.listBound);
        inputPanel.add(this.listPort);
        inputPanel.add(this.connect);

        mainPanel.add(inputPanel, BorderLayout.NORTH);
        mainPanel.add(distPanel, BorderLayout.CENTER);
        mainPanel.add(textPanel, BorderLayout.SOUTH);
        this.messages.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        this.messages.setSize((this.gui.getSize().width), (this.gui.getSize().height) / 2);
        this.messages.setHorizontalAlignment(SwingConstants.CENTER);
        this.messages.setForeground(Color.WHITE);
        this.messages.setOpaque(true);
        this.messages.setBackground(Color.BLACK);

        this.open.addActionListener(e -> {
            new Thread(() -> {

                this.ser.sendData("OPEN");
            }).start();
        });

        this.stop.addActionListener(e -> {
            new Thread(() -> {
                this.ser.sendData("CLOSE");

            }).start();
        });

        this.connect.addActionListener(e -> {

            this.ser.setDataRate(Integer.valueOf((String) this.listBound.getSelectedItem()));
            this.connect.setEnabled(false);
            this.ser.setPortName((String) this.listPort.getSelectedItem());
            this.ser.start();

            this.messages.setText("Waiting Arduino for rebooting...");
            try {
                Thread.sleep(4000);
            } catch (final InterruptedException e1) {
                e1.printStackTrace();
            }
            this.messages.setText("Ready.");
        });
        if (SerialUtil.serialsName().isEmpty()) {
            this.connect.setEnabled(false);
            this.messages.setText("NO Serial port found!");
        }

        this.gui.getContentPane().add(mainPanel);
        this.gui.setVisible(true);
    }

    @Override
    public void setMessage(final String message) {
        this.currentDist = message;
        try {
            Float.parseFloat(message);
            SwingUtilities.invokeLater(() -> {
                this.distance.setText("Distance: " + message);
            });
        } catch (final Exception e) {
            SwingUtilities.invokeLater(() -> {
                this.messages.setText(this.currentDist);
            });
        }

    }
}
