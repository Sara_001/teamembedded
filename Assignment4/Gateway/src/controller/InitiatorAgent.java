package controller;

import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import device.ButtonsEventType;
import device.ObservableButton;
import device.ObservableTimer;
import model.ButtonEvent;
import model.Event;
import model.MsgEvent;
import model.MsgSerialExchange;
import model.SerialMonitor;
import model.SimpleMsg;
import model.TickMessage;

public class InitiatorAgent extends ReactiveAgent {

  private ReactiveAgent         communicator;

  private final SerialMonitor   ser;
  private boolean               dataSent;
  private List<String>          ports;
  private final ObservableTimer timer;
  private int                   index;
  private ObservableButton      buttOn;
  private State                 state;
  private static final Logger   LOG             = Logger.getLogger(InitiatorAgent.class);
  private static final int      BOUND           = 9600;
  private static final int      DISCONNECT_TIME = 4000;
  private static final int      RETRY_TIME      = 4000;
  private static final int      TEST_TIME       = 4000;
  private static final String   ACK_MESSAGE     = "Connected";
  private static final String   START_MSG       = "Connect";
  private static final String   NAME            = "Initiator";

  private enum State {
    TRYING, CONNECTED, DISCONNECTED;

  }

  /**
   * .
   *
   */
  public InitiatorAgent() {

    super(InitiatorAgent.NAME);
    this.dataSent = false;
    this.index = 0;
    this.state = State.DISCONNECTED;
    this.timer = new ObservableTimer();

    this.ser = new SerialMonitor(null);
    this.ser.setDataRate(InitiatorAgent.BOUND);
    this.ports = null;
    InitiatorAgent.LOG.setLevel(Level.DEBUG);
  }

  /**
   * .
   *
   */
  public void init(final ObservableButton buttOn, final ReactiveAgent agent) {

    this.buttOn = buttOn;
    this.communicator = agent;
    this.buttOn.addObserver(this);
  }

  @Override
  protected void processEvent(final Event ev) {

    switch (this.state) {

      case TRYING:
        this.trying(ev);
        break;

      case DISCONNECTED:
        this.connect(ev);
        break;

      case CONNECTED:
        break;

      default:
        InitiatorAgent.LOG.error("Strange case!");
        break;
    }

  }

  private void retry() {

    this.timer.setEvent(new ButtonEvent(ButtonsEventType.ON, this.buttOn));
    this.timer.scheduleTick(InitiatorAgent.RETRY_TIME);
  }

  private boolean serchPorts() {

    this.ports = this.ser.serialsName();
    return !this.ports.isEmpty();
  }

  private Optional<String> next() {

    if (this.index != 0 && this.index <= this.ports.size()) {
      return Optional.of(this.ports.get(this.index - 1));
    } else {
      return Optional.empty();
    }

  }

  private void connect(final Event ev) {

    if (ev instanceof ButtonEvent) {
      final ButtonsEventType type = ((ButtonEvent) ev).getType();

      if (type.equals(ButtonsEventType.ON)) {
        this.timer.addObserver(this);
        System.out.println("Start EV");
        if (this.serchPorts()) {
          this.reset();
          this.tryPorts(this.next().get());
          this.state = State.TRYING;
          this.ser.addObserver(this);
          this.buttOn.removeObserver(this);
          this.sendMsgTo(this.communicator, ev);
        } else {
          System.out.println("No ports");
          this.retry();
        }
      }
    }
  }

  private void tryPorts(final String name) {

    if (this.ser.tryPortName(name)) {

      this.ser.setPortName(name);
      this.ser.start();
      this.timer.setEvent(new MsgEvent(new TickMessage(name, System.currentTimeMillis())));
      this.timer.scheduleTick(InitiatorAgent.TEST_TIME);
      System.out.println("Test port :" + name);
    } else {
      System.out.println("Port" + name + " :Fail!");
    }
  }

  private void trying(final Event ev) {

    if (ev instanceof MsgEvent && (((MsgEvent) ev).getMsg()) instanceof TickMessage) {

      if (!this.dataSent) {

        this.ser.sendData(InitiatorAgent.START_MSG);
        this.timer.setEvent(new MsgEvent((((MsgEvent) ev).getMsg())));

        this.timer.scheduleTick(InitiatorAgent.TEST_TIME);

        this.dataSent = true;

      } else {

        this.dataSent = false;

        this.index++;
        final Optional<String> name = this.next();

        this.ser.close();

        if (name.isPresent()) {

          this.tryPorts(name.get());
        } else {
          this.state = State.DISCONNECTED;
          System.out.println("No compatible ports found!");
          this.retry();
        }
      }
    } else if (ev instanceof MsgEvent && (((MsgEvent) ev).getMsg()) instanceof SimpleMsg) {
      System.out.println("Mess EV: " + ((SimpleMsg) ((MsgEvent) ev).getMsg()).getMessage());
      if (((SimpleMsg) ((MsgEvent) ev).getMsg()).getMessage().equals(InitiatorAgent.ACK_MESSAGE)) {
        this.state = State.CONNECTED;
        this.ser.removeObserver(this);
        this.sendMsgTo(this.communicator, new MsgEvent(new MsgSerialExchange(this.ser)));
        System.out.println("SUCCESS!");

      }
    } else if (ev instanceof ButtonEvent) {
      final ButtonsEventType type = ((ButtonEvent) ev).getType();
      if (type.equals(ButtonsEventType.OFF)) {
        this.timer.removeObserver(this);
        this.ser.close();
        this.state = State.DISCONNECTED;
        System.out.println("End  EV");
        this.sendMsgTo(this.communicator, new ButtonEvent(ButtonsEventType.ON, this.buttOn));
        this.scheduleTask(() -> {
          System.out.println("                             Serial CLOSED");
          this.buttOn.addObserver(this);
          this.reset();
        }, InitiatorAgent.DISCONNECT_TIME);
      }
    }
  }

  private void reset() {

    this.dataSent = false;
    this.index = 1;
  }

  private void scheduleTask(final Runnable r, final int delay) {

    new Timer().schedule(new TimerTask() {

      @Override
      public void run() {

        r.run();

      }
    }, delay);
  }

}
