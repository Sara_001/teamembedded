package controller;

import model.Event;

public abstract class ReactiveAgent extends BasicEventLoopController {

  protected ReactiveAgent(final int size) {

    super(size);
  }

  protected ReactiveAgent(final int size, final String name) {

    super(size, name);
  }

  protected ReactiveAgent(final String name) {

    super(name);
  }

  protected boolean sendMsgTo(final ReactiveAgent agent, final Event ev) {

    return agent.notifyEvent(ev);
  }
}
