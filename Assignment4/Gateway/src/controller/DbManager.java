package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class DbManager {
  private final String       regex;
  private final List<String> authenticationDatalist;
  private final File         roomInfoStorage;
  private final File         logFile;

  public DbManager(final File authenticationDataStorage, final File roomInfoStorage,
      final File logFile, final String regex) {

    this.regex = regex;
    this.logFile = logFile;
    this.roomInfoStorage = roomInfoStorage;
    this.authenticationDatalist = new ArrayList<>();
    try (final BufferedReader br = new BufferedReader(new FileReader(authenticationDataStorage))) {

      String currentLine;

      while ((currentLine = br.readLine()) != null) {
        this.authenticationDatalist.add(currentLine);
      }

    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  public boolean logInCheck(final String authenticationData) {

    return this.authenticationDatalist.contains(authenticationData);
  }

  public boolean logInCheck(final String username, final String pwd) {

    return this.logInCheck(username + this.regex + pwd);
  }

  String getUsername(final String authenticationData) {

    return authenticationData.split(this.regex)[0];
  }

  public void updateRoomInfo(final String info) {

    final String temp = info.split(this.regex)[0];
    final String led = info.split(this.regex)[1];
    this.writeFile(this.roomInfoStorage, temp + "° " + led + "%", false);
  }

  public void updateAccessLog(final String log) {

    this.writeFile(this.logFile, log, true);

  }

  private synchronized void writeFile(final File file, final String line, final boolean append) {

    final String newLine = System.getProperty("line.separator");
    PrintWriter printWriter = null;
    try {
      if (!file.exists()) {
        file.createNewFile();
      }

      printWriter = new PrintWriter(new FileOutputStream(file.getName(), append));
      printWriter.write(newLine + line);
    } catch (final IOException ioex) {
      ioex.printStackTrace();
    } finally {
      if (printWriter != null) {
        printWriter.flush();
        printWriter.close();
      }
    }
  }

}
