package model;

import java.util.HashSet;
import java.util.Set;

public class Observable {

  private final Set<Observer> observers;

  protected Observable() {

    this.observers = new HashSet<>();
  }

  protected void notifyEvent(final Event ev) {

    synchronized (this.observers) {
      for (final Observer obs : this.observers) {
        obs.notifyEvent(ev);
      }
    }
  }

  /**
   * Add one observer to the set.
   *
   * @param obs
   *          The observer to add.
   */
  public void addObserver(final Observer obs) {

    synchronized (this.observers) {
      this.observers.add(obs);
    }
  }

  /**
   * Remove one observer to the set.
   *
   * @param obs
   *          The observer to remove.
   */
  public void removeObserver(final Observer obs) {

    synchronized (this.observers) {
      this.observers.remove(obs);
    }
  }

}
