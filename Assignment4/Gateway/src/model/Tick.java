package model;

public class Tick implements Event {

  private final long time;

  public Tick(final long time) {

    this.time = time;
  }

  public long getTime() {

    return this.time;
  }
}
