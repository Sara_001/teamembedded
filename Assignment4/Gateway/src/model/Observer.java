package model;

public interface Observer {
  /**
   * Do the action for the specify event.
   *
   * @param ev
   *          the event to elaborate.
   * @return true if this event can be elaborate.
   */
  boolean notifyEvent(Event ev);
}
