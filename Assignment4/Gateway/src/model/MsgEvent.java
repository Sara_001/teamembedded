package model;

import controller.ReactiveAgent;

public class MsgEvent implements Event {

  private final Msg     msg;

  private ReactiveAgent from;

  /**
   * .
   *
   * @param msg
   *          D
   * @param from
   *          D
   */
  public MsgEvent(final Msg msg, final ReactiveAgent from) {

    this.msg = msg;
    this.from = from;
  }

  public MsgEvent(final Msg msg) {

    this.msg = msg;
  }

  public Msg getMsg() {

    return this.msg;
  }

  public ReactiveAgent getFrom() {

    return this.from;
  }
}
