package device;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

public class SimpleLed implements Led {

  private static final int           PULSE_SPEED = 50;
  private boolean                    state;
  private final GpioPinDigitalOutput led;

  /**
   * Create a new led, on the GPIO pin.
   *
   * @param pin
   *          the GPIO pin.
   *
   */
  public SimpleLed(final int pin) {

    this.led = GpioFactory.getInstance().provisionDigitalOutputPin(RaspiPin.getPinByAddress(pin));
    this.state = false;

  }

  @Override
  public void blink() {

    this.led.pulse(SimpleLed.PULSE_SPEED);
  }

  @Override
  public void changeState(final boolean set) {

    this.led.setState(set);
    this.state = set;
  }

  @Override
  public boolean isOn() {

    return this.state;
  }
}
