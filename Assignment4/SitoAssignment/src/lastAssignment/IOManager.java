package lastAssignment;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class IOManager {	
	File file; 
	
	public IOManager(String pathname) {
		file = new File(pathname);
		
	}
	

	public void write(Double brightness, Double temperature) 
			  throws IOException {
				System.out.println(file.getName());
				
				
			    FileWriter fileWriter = new FileWriter(file);
			    PrintWriter printWriter = new PrintWriter(fileWriter);
			    
			    printWriter.print(temperature + "° " + brightness + "%");
			    printWriter.close();
			}
	
}
