<?php
$myfile = fopen("/home/sara/Scrivania/Nuovo", "r") or die("Impossibile aprire il file!");
$stringa =  explode(" ",fread($myfile,filesize("/home/sara/Scrivania/Nuovo")));
   $temp = $stringa[0];
   $led = $stringa[1]; 

fclose($myfile);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="style.css">
    <script src="jquery-1.11.3.min.js" type="text/javascript"></script>
   <script src="refresh.js" type="text/javascript"></script> 

  </head>
  <body>
  <div class="container">
    <div class="card temp">
        <div class="icon">
        <img src="tmp.png" alt="">
      </div>
        <div class="inner">
            <div class="title">
              <div class="text">TEMPERATURE</div>
            </div>
            <div class="number" id="temp"><?php echo $temp; ?></div>
          </div>
    </div>
    <div class="card  led">
        <div class="icon">
        <img src="led.png" alt="">
      </div>
        <div class="inner">
            <div class="title">
              <div class="text">BRIGHTNESS</div>
            </div>
            <div class="number" id="led"><?php echo $led; ?> </div>
          </div>
    </div>  </body>
  </div>
  <div class="refresh">
    <a href="index.php"><button  class="button" type="button" name="button">Refresh</button></a>

  </div>
</html>
