package controller;

import java.time.LocalDateTime;

import org.apache.log4j.Logger;

import device.ObservableTimer;
import model.Event;
import model.Msg;
import model.MsgEvent;
import model.MsgSerialExchange;
import model.SerialMonitor;
import model.SimpleMsg;
import model.TickMessage;

public class CommunicatorAgent extends ReactiveAgent {
  private static final String   NAME             = "Communicator";
  private static final int      TIMER_DELAY      = 500;
  private static final String   AUTH_SUCCESS_MSG = "Access allowed ";
  private static final String   AUTH_FAILURE_MSG = "Access denied ";
  private static final String   USER_IN          = "User successfully got in ";
  private static final String   FAILURE_MSG      = "Time expired, access failure ";
  private ReactiveAgent         deviceAgent;
  private ReactiveAgent         monitorAgent;
  private final ObservableTimer timer;
  private final DbManager       manager;
  private State                 state;
  private static final Logger   LOG              = Logger.getLogger(CommunicatorAgent.class);
  private SerialMonitor         ser;

  private enum State {
    OFF("OFF"), NOT_LOGGED("NOT_LOGGED"), LOGGED("LOGGED"), LOGGING("LOGGING");
    String name;

    private State(final String name) {

      this.name = name;
    }

    String getName() {

      return this.name;

    }
  }

  public enum MessageType {
    AUTH_DATA("<0>"), INFO_ROOM("<1>"), CLOSE("<2>"), FAILURE("<3>"), SUCCESS("<4>");
    String messageTypeIdentifier;

    private MessageType(final String messageTypeIdentifier) {

      this.messageTypeIdentifier = messageTypeIdentifier;
    }

    String getMessageTypeIdentifier() {

      return this.messageTypeIdentifier;

    }

  }

  protected CommunicatorAgent(final DbManager manager) {

    super(CommunicatorAgent.NAME);
    this.manager = manager;
    this.timer = new ObservableTimer();
    this.timer.addObserver(this);
    this.state = State.OFF;

  }

  /**
   * .
   *
   */
  public void init(final ReactiveAgent deviceAgent, final ReactiveAgent monitorAgent) {

    this.deviceAgent = deviceAgent;
    this.monitorAgent = monitorAgent;

  }

  @Override
  protected void processEvent(final Event ev) {

    if (ev instanceof MsgEvent) {
      final MsgEvent ms = (MsgEvent) ev;

      if (ms.getMsg() instanceof SimpleMsg && !this.state.equals(State.OFF)) {

        final String messageString = ((SimpleMsg) ms.getMsg()).getMessage();

        switch (this.getType(messageString)) {

          case INFO_ROOM:
            this.sendMsgTo(this.monitorAgent, new MsgEvent(new SimpleMsg(messageString
                .substring(MessageType.INFO_ROOM.getMessageTypeIdentifier().length()))));
            break;

          case AUTH_DATA:
            if (this.state.equals(State.NOT_LOGGED)) {
              this.login(messageString
                  .substring(MessageType.AUTH_DATA.getMessageTypeIdentifier().length()));
            }
            break;

          case FAILURE:
            if (this.state.equals(State.LOGGING)) {
              this.state = State.NOT_LOGGED;
              this.sendMsgTo(this.deviceAgent, new MsgEvent(
                  new SimpleMsg(DeviceAgent.MessageType.ACCESS_DENIED.getStringValue())));
              this.manager.updateAccessLog(LocalDateTime.now().toString() + " "
                  + CommunicatorAgent.FAILURE_MSG + "USER: " + messageString
                      .substring(MessageType.FAILURE.getMessageTypeIdentifier().length()));
              this.ser.sendData("Not logged");
            }
            break;

          case SUCCESS:
            if (this.state.equals(State.LOGGING)) {
              this.state = State.LOGGED;
              this.sendMsgTo(this.deviceAgent, new MsgEvent(
                  new SimpleMsg(DeviceAgent.MessageType.ACCESS_ALLOWED.getStringValue())));
              this.manager.updateAccessLog(LocalDateTime.now().toString() + " "
                  + CommunicatorAgent.USER_IN + "USER: " + messageString
                      .substring(MessageType.SUCCESS.getMessageTypeIdentifier().length()));
              this.ser.sendData("Logged");
            }
            break;

          case CLOSE:
            if (this.state.equals(State.LOGGED)) {
              this.logout();
            }
            break;

          default:
            CommunicatorAgent.LOG.error("Strange case");
            break;
        }

      } else if (!(ms.getMsg() instanceof SimpleMsg) && this.state.equals(State.OFF)) {
        this.switchOn(ms);
      }

    }
  }

  private void logout() {

    this.state = State.NOT_LOGGED;
    this.sendMsgTo(this.deviceAgent,
        new MsgEvent(new SimpleMsg(DeviceAgent.MessageType.EXIT.getStringValue())));
    this.ser.sendData("Session closed");

  }

  private void login(final String authenticationData) {

    if (this.manager.logInCheck(authenticationData)) {
      this.state = State.LOGGING;
      this.manager.updateAccessLog(LocalDateTime.now().toString() + " "
          + CommunicatorAgent.AUTH_SUCCESS_MSG + this.manager.getUsername(authenticationData));
      this.ser.sendData(CommunicatorAgent.AUTH_SUCCESS_MSG);

    } else {
      this.sendMsgTo(this.deviceAgent,
          new MsgEvent(new SimpleMsg(DeviceAgent.MessageType.ACCESS_DENIED.getStringValue())));
      this.manager.updateAccessLog(LocalDateTime.now().toString() + " "
          + CommunicatorAgent.AUTH_FAILURE_MSG + this.manager.getUsername(authenticationData));
      this.ser.sendData(CommunicatorAgent.AUTH_FAILURE_MSG);

    }
  }

  private void switchOn(final MsgEvent msgEv) {

    final Msg msg = msgEv.getMsg();

    switch (msg.getType()) {
      case MsgSerialExchange.TYPE:
        System.out.println("Exchanged serial");
        this.timer.addObserver(this);
        this.ser = ((MsgSerialExchange) msg).getSer();
        this.ser.sendData("connected");
        this.setTimer(CommunicatorAgent.TIMER_DELAY);
        break;

      case TickMessage.TYPE:

        System.out.println("connected");
        this.ser.addObserver(this);
        this.state = State.NOT_LOGGED;
        // this.setTimer(CommunicatorAgent.TIMER_DELAY);////??

        break;

      default:
        CommunicatorAgent.LOG.error("Wrong msg event");
        break;
    }

  }

  private void setTimer(final int delay) {

    this.timer
        .setEvent(new MsgEvent(new TickMessage(this.state.getName(), System.currentTimeMillis())));
    this.timer.scheduleTick(delay);
  }

  private MessageType getType(final String msg) {

    final String reg = msg.substring(0, 3);
    for (final MessageType type : MessageType.values()) {
      if (type.getMessageTypeIdentifier().equals(reg)) {
        return type;
      }
    }
    return null;

  }

}
