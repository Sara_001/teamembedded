package controller;

import org.apache.log4j.Logger;

import model.Event;
import model.MsgEvent;
import model.SimpleMsg;

public class RoomMonitorAgent extends ReactiveAgent {
  DbManager                   manager;
  private static final Logger LOG = Logger.getLogger(CommunicatorAgent.class);

  protected RoomMonitorAgent(final int size, final String name, final DbManager manager) {

    super(size, name);
    this.manager = manager;

  }

  @Override
  protected void processEvent(final Event ev) {

    if (ev instanceof MsgEvent) {
      final MsgEvent ms = (MsgEvent) ev;

      if (ms.getMsg() instanceof SimpleMsg) {

        final String messageString = ((SimpleMsg) ms.getMsg()).getMessage();
        this.manager.updateRoomInfo(messageString);

      } else {

        RoomMonitorAgent.LOG.error("Strange case");
      }

    }

  }

}
