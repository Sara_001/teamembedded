package controller;

import org.apache.log4j.Logger;

import device.Led;
import device.SimpleLed;
import model.Event;
import model.MsgEvent;
import model.SimpleMsg;

public class DeviceAgent extends ReactiveAgent {

  private static final Logger LOG  = Logger.getLogger(DeviceAgent.class);
  private final Led           inside;
  private static final String NAME = "DeviceAgent";
  private final Led           failed;

  private Position            state;

  public enum Position {
    INSIDE, OUTSIDE;
  }

  public enum MessageType {
    ACCESS_DENIED("ACCESS_DENIED"), ACCESS_ALLOWED("ACCESS_ALLOWED"), EXIT("EXIT");
    String stringValue;

    private MessageType(final String stringValue) {

      this.stringValue = stringValue;
    }

    String getStringValue() {

      return this.stringValue;

    }
  }

  protected DeviceAgent(final int led1, final int led2) {

    super(DeviceAgent.NAME);
    this.state = Position.OUTSIDE;
    this.inside = new SimpleLed(led1);
    this.failed = new SimpleLed(led2);
  }

  /**
   * .
   *
   */
  public void init() {

    this.inside.changeState(false);
    this.failed.changeState(false);

  }

  @Override
  protected void processEvent(final Event ev) {

    if (ev instanceof MsgEvent && (((MsgEvent) ev).getMsg()) instanceof SimpleMsg) {
      final MessageType message = MessageType
          .valueOf(((SimpleMsg) ((MsgEvent) ev).getMsg()).getMessage());
      switch (this.state) {

        case INSIDE:
          if (message == MessageType.EXIT) {
            this.inside.changeState(false);
            this.state = Position.OUTSIDE;
          }
          break;

        case OUTSIDE:
          if (message == MessageType.ACCESS_ALLOWED) {
            this.inside.changeState(true);
            this.state = Position.INSIDE;
          } else if (message == MessageType.ACCESS_DENIED) {
            this.failed.blink();
          } else {
            DeviceAgent.LOG.warn("Can not exit if you're outside");
          }
          break;

        default:
          DeviceAgent.LOG.warn("Strange case!");
          break;
      }
    }
  }

}
