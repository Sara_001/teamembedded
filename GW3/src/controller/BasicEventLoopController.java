package controller;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import model.Event;
import model.Observable;
import model.Observer;

public abstract class BasicEventLoopController extends Thread implements Observer {
  public static final int        DEFAULTEVENTQUEUESIZE = 50;

  protected BlockingQueue<Event> eventQueue;

  protected BasicEventLoopController(final int size) {

    this.eventQueue = new ArrayBlockingQueue<>(size);
  }

  protected BasicEventLoopController(final int size, final String name) {

    this(size);
    super.setName(name);
  }

  protected BasicEventLoopController() {

    this(BasicEventLoopController.DEFAULTEVENTQUEUESIZE);
  }

  protected BasicEventLoopController(final String name) {

    this();
    super.setName(name);
  }

  protected abstract void processEvent(Event ev);

  @Override
  public void run() {

    while (true) {
      try {
        final Event ev = this.waitForNextEvent();
        this.processEvent(ev);
      } catch (final Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  protected void startObserving(final Observable object) {

    object.addObserver(this);
  }

  protected void stopObserving(final Observable object) {

    object.removeObserver(this);
  }

  protected Event waitForNextEvent() throws InterruptedException {

    return this.eventQueue.take();
  }

  protected Event pickNextEventIfAvail() throws InterruptedException {

    return this.eventQueue.poll();
  }

  protected void clearAllEvent() {

    this.eventQueue.clear();
  }

  @Override
  public boolean notifyEvent(final Event ev) {

    return this.eventQueue.offer(ev);
  }
}
