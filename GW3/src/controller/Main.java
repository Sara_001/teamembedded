package controller;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import device.ButtonsEventType;
import device.ObservableButton;

public final class Main {
  private static final Logger LOG   = Logger.getLogger(Main.class);
  private static final String regex = "???";

  private Main() {

  }

  /**
   * .
   *
   * @param args
   *          d
   */
  public static void main(final String[] args) {

    final ObservableButton buttOn = new ObservableButton(0, ButtonsEventType.ON);
    final InitiatorAgent initAgent = new InitiatorAgent();
    final DbManager manager = new DbManager(new File("authenticationData.txt"),
        new File("roomInfo.txt"), new File("accessLog.txt"), Main.regex);
    final CommunicatorAgent communicatorAgent = new CommunicatorAgent(manager);
    final DeviceAgent deviceAgent = new DeviceAgent(4, 5);
    final RoomMonitorAgent monitorAgent = new RoomMonitorAgent(50, "Monitor", manager);

    initAgent.init(buttOn, communicatorAgent);
    communicatorAgent.init(deviceAgent, monitorAgent);

    initAgent.start();
    communicatorAgent.start();
    deviceAgent.start();
    deviceAgent.init();
    monitorAgent.start();
    Main.LOG.setLevel(Level.DEBUG);
    Main.LOG.debug("START!");
  }

}
