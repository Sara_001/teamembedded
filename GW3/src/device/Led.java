package device;

public interface Led {

  void changeState(final boolean set);

  void blink();

  boolean isOn();

}
