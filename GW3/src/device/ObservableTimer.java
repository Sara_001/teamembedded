package device;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import model.Event;
import model.Observable;
import model.Tick;

public class ObservableTimer extends Observable {

  private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);

  private ScheduledFuture<?>             tickHandle;

  private final Runnable                 tickTask;

  private Event                          ev;

  /**
   * Create a new Timer, it can launch task after delta-seconds.
   */
  public ObservableTimer() {

    this.ev = new Tick(System.currentTimeMillis());
    this.tickTask = () -> {

      this.notifyEvent(this.ev);
    };
  }

  /**
   * Start generating tick event.
   *
   * @param period
   *          period in milliseconds
   */
  public void start(final long period) {

    this.tickHandle = this.scheduler.scheduleAtFixedRate(this.tickTask, 0, period,
        TimeUnit.MILLISECONDS);
  }

  /**
   * Generate a tick event after a number of milliseconds.
   *
   */
  public void scheduleTick(final long deltat) {

    this.scheduler.schedule(this.tickTask, deltat, TimeUnit.MILLISECONDS);
  }

  /**
   * Stop generating tick event.
   *
   */
  public void stop() {

    if (this.tickHandle != null) {
      this.tickHandle.cancel(false);
      this.tickHandle = null;
    }
  }

  public void setEvent(final Event inEv) {

    this.ev = inEv;
  }
}
