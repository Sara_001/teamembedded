package device;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import model.ButtonEvent;
import model.Observable;

public class ObservableButton extends Observable implements GpioPinListener, Button {

  private final GpioPinDigitalInput button;

  /**
   * Create a new button on the GPIO pin, whit the type insert.
   *
   * @param pin
   *          The GPIO pin for the button.
   * @param type
   *          the type of the button.
   */
  public ObservableButton(final int pin, final ButtonsEventType type) {

    this.button = GpioFactory.getInstance().provisionDigitalInputPin(RaspiPin.getPinByAddress(pin),
        PinPullResistance.PULL_DOWN);
    this.button.addListener((GpioPinListenerDigital) lisner -> {
      if (lisner.getState() == PinState.HIGH) {
        this.notifyEvent(new ButtonEvent(type, this));
      }

    });
  }

  @Override
  public boolean isPressed() {

    return this.button.isHigh();
  }

}
