package model;

public class MsgSerialExchange implements Msg {
  private final SerialMonitor ser;
  public static final String  TYPE = "Serial";

  public MsgSerialExchange(final SerialMonitor ser) {

    this.ser = ser;
  }

  public SerialMonitor getSer() {

    return this.ser;
  }

  @Override
  public String getType() {

    return MsgSerialExchange.TYPE;
  }

}
