package model;

import device.Button;

public class ButtonPressed implements Event {
  private final Button source;

  public ButtonPressed(final Button source) {

    this.source = source;
  }

  public Button getSourceButton() {

    return this.source;
  }
}
