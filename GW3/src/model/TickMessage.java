package model;

public class TickMessage implements Msg {
  public static final String TYPE = "Tick";
  private final String       msg;
  private final Long         time;

  /**
   * A new Timer message.
   *
   * @param msg
   *          The string message to send.
   *
   * @param time
   *          The actual time.
   */
  public TickMessage(final String msg, final Long time) {

    this.msg = msg;
    this.time = time;
  }

  public String getMsg() {

    return this.msg;
  }

  public Long getTime() {

    return this.time;
  }

  @Override
  public String getType() {

    return TickMessage.TYPE;
  }

}
