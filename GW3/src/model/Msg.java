package model;

public interface Msg {
  String getType();
}
