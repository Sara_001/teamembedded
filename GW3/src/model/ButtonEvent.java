package model;

import device.Button;
import device.ButtonsEventType;

public class ButtonEvent implements Event {
  private final Button           source;
  private final ButtonsEventType type;

  /**
   * Represent one button event.
   *
   * @param type
   *          The type of this event.
   * @param source
   *          The source button.
   */
  public ButtonEvent(final ButtonsEventType type, final Button source) {

    this.type = type;
    this.source = source;
  }

  public Button getSourceButton() {

    return this.source;
  }

  public ButtonsEventType getType() {

    return this.type;
  }

}
