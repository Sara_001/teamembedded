package model;

public class SimpleMsg implements Msg {
  private final String       message;
  public static final String TYPE = "Simple";

  public SimpleMsg(final String message) {

    this.message = message;
  }

  public String getMessage() {

    return this.message;
  }

  @Override
  public String getType() {

    return SimpleMsg.TYPE;
  }

}
