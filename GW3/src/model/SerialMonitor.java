package model;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Simple Serial Monitor, adaptation from:
 * http://playground.arduino.cc/Interfacing/Java.
 *
 */
public class SerialMonitor implements SerialPortEventListener {
  private SerialPort          serialPort;

  /**
   * A BufferedReader which will be fed by a InputStreamReader converting the.
   * bytes into characters making the displayed results codepage independent.
   */
  private BufferedReader      input;

  /** The output stream to the port. */
  private OutputStream        output;         // NOPMD

  /** Milliseconds to block while waiting for port open. */
  private static final int    TIME_OUT = 2000;
  private static final int    RATE     = 9600;
  private final Set<Observer> set;
  private String              portName;
  private int                 dataRate;

  /**
   * Constructor.
   *
   * @param portName
   *          the {@link String} port name
   */
  public SerialMonitor(final String portName) {

    this.dataRate = SerialMonitor.RATE;
    this.set = new HashSet<>();
    this.portName = portName;
  }

  /**
   * Constructor.
   *
   * @param portName
   *          the {@link String} port name
   * @param dataRate
   *          the data rate
   */
  public SerialMonitor(final String portName, final int dataRate) {

    this.dataRate = dataRate;
    this.portName = portName;
    this.set = new HashSet<>();
  }

  /**
   * Start method.
   *
   */
  public void start() {

    CommPortIdentifier portId = null;

    try {
      portId = CommPortIdentifier.getPortIdentifier(this.portName);
      this.serialPort = (SerialPort) portId.open(this.getClass().getName(), SerialMonitor.TIME_OUT);

      // set port parameters
      this.serialPort.setSerialPortParams(this.dataRate, SerialPort.DATABITS_8,
          SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

      // open the streams
      this.input = new BufferedReader(new InputStreamReader(this.serialPort.getInputStream()));
      this.output = this.serialPort.getOutputStream();

      // add event listeners
      this.serialPort.addEventListener(this);
      this.serialPort.notifyOnDataAvailable(true);

    } catch (final Exception e) {
      System.err.println(e.toString());
    }
  }

  /**
   * This should be called when you stop using the port. This will prevent port
   * locking on platforms like Linux.
   */
  public void close() {

    synchronized (this.serialPort) {
      if (this.serialPort != null) {
        this.serialPort.removeEventListener();
        this.serialPort.close();
      }
    }
  }

  public void addObserver(final Observer obs) {

    this.set.add(obs);
  }

  public void removeObserver(final Observer obs) {

    this.set.remove(obs);
  }

  /**
   * Handle an event on the serial port. Read the data and print it.
   */
  @Override
  public synchronized void serialEvent(final SerialPortEvent oevent) {

    if (oevent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
      try {
        if (this.input.ready()) {
          final String inputLine = this.input.readLine();
          this.set.forEach(x -> x.notifyEvent(new MsgEvent(new SimpleMsg(inputLine))));
        }
      } catch (final Exception e) {
        System.err.println(e.toString());
      }
    }
    // Ignore all the other eventTypes, but you should consider the other ones.
  }

  /**
   * Return all alctual serial port name.
   *
   * @return {@link List} contain all names.
   */
  public List<String> serialsName() {

    final List<String> lis = new ArrayList<>();
    final Enumeration<?> portEnum = CommPortIdentifier.getPortIdentifiers();
    while (portEnum.hasMoreElements()) {
      final CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
      lis.add(currPortId.getName());
    }
    return lis;
  }

  /**
   * Send data.
   *
   * @param data
   *          the String data
   */
  public void sendData(final String data) {

    try {

      this.output.write(data.concat("\n").getBytes(Charset.forName("UTF-8")));
      this.output.flush();
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Set the port name.
   *
   * @param portName
   *          the {@link String} port name
   */
  public void setPortName(final String portName) {

    this.portName = portName;
  }

  /**
   * Set the port name.
   *
   * @param port
   *          the {@link String} port name
   */
  public boolean tryPortName(final String port) {

    try {
      final CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(port);
      final SerialPort test = (SerialPort) portId.open(this.getClass().getName(),
          SerialMonitor.TIME_OUT);
      if (test != null) {
        test.close();
      }
      return true;
    } catch (final Exception e) {
      e.printStackTrace();
      return false;
    }

  }

  /**
   * Set the bauld rate.
   *
   * @param dataRate
   *          the bauld rate
   */
  public void setDataRate(final int dataRate) {

    this.dataRate = dataRate;
  }

  /**
   * Get the port name.
   *
   * @return the port name
   */
  public String getPortName() {

    return this.portName;
  }

  /**
   * Get the data rate.
   *
   * @return the data rate
   */
  public int getDataRate() {

    return this.dataRate;
  }

}