#include "Interaction.h"

#define MAX_BRT 255
#define STR_FADE 5
#define STR_PULSE_SPEED 200 //Velocit� definita per evitare che il gioco iniszi senza fade o con degli scatti del led
#define PULSE_DIVIDE 10
#define POTENTIOMETER_MAX 1023
#define LEVELS_MULTI 50
#define DELAY_DEBOUNCE 200

int fadeAmount = STR_FADE;
int brightness = 0;
int pulseSpeed = STR_PULSE_SPEED; // Decrementa maggiore � la velocit�

void fade(int led) {
  analogWrite(led, brightness);
  brightness = brightness + fadeAmount;
  if (brightness <= 0 || brightness >= MAX_BRT) {
    fadeAmount = -fadeAmount;
  }
  delay(pulseSpeed / PULSE_DIVIDE);
}

void setSpeed(unsigned char regolator , int levels) {

  pulseSpeed = LEVELS_MULTI*map(analogRead(regolator), 0, POTENTIOMETER_MAX ,1, levels);

}


int getPulseSpeed(){
	return pulseSpeed;
}

void pulse(int nled) {
  digitalWrite(nled, HIGH);
  delay(pulseSpeed);
  digitalWrite(nled, LOW);
  delay(pulseSpeed);
}

bool debounceInput(int button) {
  if (digitalRead(button)) {
  //  Serial.print("Bottone Premuto:");
  //  Serial.println(button - 1);
    delay(DELAY_DEBOUNCE);

    return true;
  }
  else return false;

}
