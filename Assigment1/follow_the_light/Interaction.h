/*
 * Cont.h
 *
 *  Created on: 20 ott 2017
 *      Author: Rad
 */

#ifndef INTERACTION_H_
#define INTERACTION_H_
#include <Arduino.h>


extern int level;
void fade(int led);
void pulse(int nled);
void setSpeed(unsigned char regolator, int levels);
int getPulseSpeed();
bool debounceInput(int button);

#endif /* INTERACTION_H_ */
