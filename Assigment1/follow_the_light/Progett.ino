#include "Interaction.h"
#include <TimerOne.h>//OPPOrtunamente modificato
#define LED_1 10
#define LED_2 11
#define LED_3 12
#define LED_FLASH 6
#define BUTT_1 2
#define BUTT_2 3
#define BUTT_3 4
#define REGOLATOR A0 //Potentiometer
#define DELAY_MUL 50
#define SERIAL_SPEED 9600
#define SECOND_TIMER 2000050
#define DIM_ARR 100
#define NUM_LEVELS 5

int seq_len = 0;
int sequence[DIM_ARR];
int indexToCheck = 0;
volatile bool loses = false;
volatile bool wins = false;

extern int level;

void loseTimer();
void printScore(int sequenceNumber);
int getScore(int sequenceNumber);
void showSequence();
void lose();
void insertSequence();
void setGameTimer();
int pressedButton();
void loseTimer();

void setup() {	Serial.println("Welcome To Follow The Light");
  Serial.begin(SERIAL_SPEED);
  pinMode(LED_1, OUTPUT);
  pinMode(LED_2, OUTPUT);
  pinMode(LED_3, OUTPUT);
  pinMode(LED_FLASH, OUTPUT);
  pinMode(BUTT_1, INPUT);
  pinMode(BUTT_2, INPUT);
  pinMode(BUTT_3, INPUT);
  pinMode(REGOLATOR, INPUT);
  setGameTimer();
  Serial.println("Welcome to Follow the Light!");
}

void loop() {
  if (debounceInput(BUTT_1)) {
    initializeGame();
    do {
      showSequence();
      insertSequence();
    } while (!loses);
  } else {
    fade(LED_FLASH);
    setSpeed(REGOLATOR,NUM_LEVELS);
  }
}


void printScore(int sequenceNumber){
  Serial.print("Game Over - Score:    ");
  Serial.println(getScore(sequenceNumber));
}

int getScore(int sequenceNumber){
	return ((NUM_LEVELS+1)-getPulseSpeed()/DELAY_MUL)*(sequenceNumber-1)*(sequenceNumber)/2;//Formula di gauss per ottenere il punteggio, moltiplicato per il numero di livello
}

void showSequence() {
  pulse(LED_FLASH);
  //Serial.println("1)Sequenza");

  analogWrite(LED_FLASH, 0);//Previene il led acceso durante il gioco

  sequence[seq_len] = random(LED_1, LED_3 + 1);
  seq_len++;
  for (int i = 0; i < seq_len; i++) {
    pulse(sequence[i]);
  }
}


void lose() {
  Timer1.stop();
  loses = true;
  printScore(seq_len);
  pulse(LED_FLASH);
  pulse(LED_FLASH);
  seq_len = 0;
}

void insertSequence() {
  //Serial.println("2)Inserimento");
  pulse(LED_FLASH);
  for (int indexToCheck=0 ;!loses && !((indexToCheck) == (seq_len)); indexToCheck++) {
    Timer1.startNoInterrupt();
 //   int x = pressedButton();
 //  Serial.print("Bottone  Atteso:");
 //   Serial.println(sequence[indexToCheck] - 9);
  //  Serial.println("--------------------");
    if ( pressedButton() != sequence[indexToCheck]) {
      lose();
    }
  }

}

void initializeGame(){
      Serial.println("Ready!");
      loses = false;
}

void setGameTimer(){
	Timer1.initialize(SECOND_TIMER);
	Timer1.attachInterrupt(loseTimer);
	Timer1.stop();
}

int pressedButton() {
  while (!loses) {
    if (debounceInput(BUTT_1)) {
      Timer1.stop();

      return LED_1;
    } else if (debounceInput(BUTT_2)) {
      Timer1.stop();

      return LED_2;
    } else if (debounceInput(BUTT_3)) {
      Timer1.stop();

      return LED_3;
    }
  }
  return -1;
}

void loseTimer() {
  loses = true;
}